## Developing for Planner
In order to develop for planner ensure you have node version 18 installed as a prerequisite. https://nodejs.org/en/
1. Open up a terminal/cmd and go to the root of the project
2. Run `npm install` to install the necessary packages to validate js.
3. Now you can simply run `npm run validate`
4. Output should look similar to below for both.
<img src="/assets/developer_output.png"></img>

Once you've committed you changes, open a Merge Request! Please include a description of what your change does, and as applicable, a screenshot of what your changes look like.

## I clicked "Request Access", why don't I have it yet?

How about you start by opening some Merge Requests? You don't need any access you don't already have to either use this plugin or contribute to its development!

